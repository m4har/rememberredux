import React from 'react';
import {Provider} from 'react-redux';
import {pure} from 'recompose';
import {Store} from './src/redux';
import Routes from './src/routes';

const Root = () => (
  <Provider store={Store}>
    <Routes />
  </Provider>
);

export default pure(Root);
