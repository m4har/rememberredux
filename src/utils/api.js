import axios from 'axios';

const baseURL = 'https://reqres.in/';

const instance = axios.create({
  baseURL,
  timeout: 1000,
  headers: {'X-Custom-Header': 'foobar'},
});

export const getUser = page => instance.get('api/users', {params: {page}});
