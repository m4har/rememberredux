import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Home from '../modules/home';

const AppNavigator = createStackNavigator({
  homeScreen: {
    screen: Home,
  },
});

export default createAppContainer(AppNavigator);
