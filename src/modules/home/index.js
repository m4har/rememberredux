import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import {connect} from 'react-redux';
import {pure} from 'recompose';
import {getUser} from './redux/action';

const Home = props => {
  const {home, getUser} = props;
  const [page, setPage] = useState(1);
  const {data, loading} = home;
  useEffect(() => {
    getUser(page);
  }, []);
  const renderItem = ({item}) => (
    <View style={{margin: 10, backgroundColor: '#f0f0f0', borderRadius: 20}}>
      <View style={{flexDirection: 'row'}}>
        <Image source={{uri: item.avatar}} style={{height: 100, width: 100}} />
        <View style={{justifyContent: 'center'}}>
          <Text>{item.first_name}</Text>
          <Text>{item.email}</Text>
        </View>
      </View>
    </View>
  );
  return (
    <View style={{flex: 1}}>
      {loading ? <Text>LOADING</Text> : null}
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toLocaleString()}
        renderItem={renderItem}
      />
    </View>
  );
};

const homeScreen = pure(Home);
const mapStateToProps = ({home}) => ({
  home,
});
const mapDispatchToProps = dispatch => ({
  getUser: payload => dispatch(getUser(payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(homeScreen);
