const initState = {
  loading: true,
  data: [],
};

export const home = (state = initState, action) => {
  switch (action.type) {
    case 'SETDATA_HOME':
      return {...state, loading: false, data: action.payload};
    case 'LOADING_HOME':
      return {...state, loading: action.payload};
    default:
      return state;
  }
};
