import {put, takeLatest} from 'redux-saga/effects';
import {getUser} from '../../../utils/api';

function* homeInfo({payload}) {
  try {
    const response = yield getUser(payload);
    yield put({type: 'SETDATA_HOME', payload: response.data.data});
    yield put({type: 'LOADING_HOME', payload: false});
  } catch (error) {
    alert(error);
  }
}

export const watcherHome = [takeLatest('HOME_REQUEST', homeInfo)];
