import {all} from 'redux-saga/effects';
import {watcherHome} from '../modules/home/redux/saga';

export default function* reduxSaga() {
  yield all([...watcherHome]);
}
