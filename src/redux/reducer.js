import {combineReducers} from 'redux';

// REDUCER DATA
import {home} from '../modules/home/redux/reducer';

const Reducer = combineReducers({
  home
});

export default Reducer;
