import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

import Reducer from './reducer';
import Saga from './Saga';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

if (process.env.NODE_ENV === 'development') {
  // middleware untuk development
  middlewares.push(logger);
}

const middleware = applyMiddleware(...middlewares);

export const Store = createStore(Reducer, middleware);

sagaMiddleware.run(Saga);
